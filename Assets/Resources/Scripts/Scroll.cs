﻿using UnityEngine;
using System.Collections;

public class Scroll : MonoBehaviour
{
	public float scrollSpeed; //Texturin scrollaamisen nopeus
	public float tileSizeX; // Alueen koko millä scrollaaminen tapahtuu X- Akselilla

	private Vector3 startPosition; // Scrollaamisen lähtökohta

	void Start ()
	{
		startPosition = transform.position; //Lähtökohta on siinä, missä objecti on
	}

	void Update ()
	{
		float newPosition = Mathf.Repeat(Time.time * scrollSpeed, tileSizeX); // Objektin uusi paikka jota muutetaan, mathf.repeat "Looppaa" arvoa Ajan, Scrollnopeuden ja X pituuden perusteella
																				
																			  
		transform.position = startPosition + Vector3.left * newPosition; // Siirtää objektin X akselilla vasemmalle edellisen laskutoimituksen perusteella
	}
}
