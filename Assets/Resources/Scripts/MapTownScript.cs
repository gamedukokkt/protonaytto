﻿using UnityEngine;
using System.Collections;

public class MapTownScript : MonoBehaviour {
    public string sceneName = "";
    void OnTriggerEnter2D(Collider2D c)
    {
        // First check if we collided with player
        if (c.CompareTag("Player"))
        {
            // Then check if the scene name is defined
            if (sceneName.Length > 0)
            {
                // If name is defined, try to load the defined level
                Application.LoadLevel(sceneName);
            }
            else
            {
                // If the name is undefined, throw error to the console
                Debug.LogError("Level name wasn't defined for " + name);
            }
        }
    }
}
